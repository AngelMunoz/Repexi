namespace Mandadinfs

module App =

    open Feliz
    open Feliz.Router
    open Elmish
    open Mandadinfs.Pages

    type State = { CurrentUrl: list<string> }

    type Msg =
        | UrlChanged of list<string>
        | ShowNotification of string



    let init () =
        { CurrentUrl = Router.currentUrl () }, Cmd.none

    let update (msg: Msg) (state: State) =
        match msg with
        | UrlChanged segments -> { state with CurrentUrl = segments }, Cmd.none
        | ShowNotification notification -> state, Cmd.none

    let render (state: State) (dispatch: Msg -> unit) =
        let buildNotFoundTitle (url: list<string>) =
            let buildUrl (previous: string) (next: string) = sprintf "%s/%s" previous next
            let url = url |> List.reduce buildUrl
            sprintf "Url Not found '%s'" url

        let mainNav =
            Html.nav
                [ prop.className "navbar"
                  prop.children
                      [ Html.section
                          [ prop.className "navbar-section"
                            prop.children
                                [ Html.a
                                    [ prop.className "btn btn-link"
                                      prop.href (Router.format ("notes"))
                                      prop.text "Notes" ]
                                  Html.a
                                      [ prop.className "btn btn-link"
                                        prop.href (Router.format ("recipes"))
                                        prop.text "Recipes" ] ] ]
                        Html.section
                            [ prop.className "navbar-center"
                              prop.children
                                  [ Html.a
                                      [ prop.className "navbar-brand"
                                        prop.href (Router.format (""))
                                        prop.text "Repexi" ] ] ]
                        Html.section [ prop.className "navbar-section" ] ] ]

        let currentPage =
            React.fragment
                [ match state.CurrentUrl with
                  | []
                  | [ "notes" ] -> Notes.page
                  | [ "recipes" ] ->
                      Html.article
                          [ prop.className "page recipes-page"
                            prop.children [ Html.h1 "Recipes" ] ]
                  | [ "recipes"; recipeid ] ->
                      Html.article
                          [ prop.className "page recipes-page"
                            prop.children [ Html.h1 "Recipes" ] ]
                  | url ->
                      Html.article
                          [ prop.className "page recipes-page"
                            prop.children [ Html.h1 (buildNotFoundTitle url) ] ] ]

        let mainFooter =
            Html.footer
                [ prop.className "footer"
                  prop.children
                      [ Html.header
                          [ Html.h5 "Repexi, a somewhat recipe app"
                            Html.p "Tunaxor Apps." ]

                        Html.section [] ] ]

        Router.router
            [ Router.onUrlChanged (UrlChanged >> dispatch)
              Router.application
                  (Html.article
                      [ prop.className "repexi"
                        prop.children [ mainNav; currentPage; mainFooter ] ]) ]
