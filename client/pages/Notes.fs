namespace Mandadinfs.Pages

module Notes =
    open System
    open Fable.Core.JS
    open Elmish
    open Feliz
    open Feliz.UseElmish
    open Mandadinfs.Types

    type private State =
        { CurrentNote: Option<Note>
          Notes: list<Note> }

    type private NoteField =
        | Title of Option<string>
        | Content of string

    type private Msg =
        | FetchNotes
        | NewNote
        | OnFieldChanged of Option<Note> * NoteField
        | SaveNote of Note
        | FetchResult of Result<list<Note>, exn>
        | SaveResult of Result<Option<Note>, exn>

    let private fetchNotes (): Promise<Result<list<Note>, exn>> = promise { return Ok([]) }
    let private saveNote (note: Note): Promise<Result<Option<Note>, exn>> = promise { return (Ok None) }

    let private init =
        let state = { CurrentNote = None; Notes = [] }

        let cmd =
            Cmd.OfPromise.perform fetchNotes () FetchResult

        state, cmd

    let private update (msg: Msg) (state: State) =
        match msg with
        | FetchNotes -> state, Cmd.OfPromise.perform fetchNotes () FetchResult
        | NewNote ->
            let note =
                { Id = Guid.NewGuid()
                  Content = ""
                  Timestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds()
                  Title = None
                  User = UserId "" }

            { state with CurrentNote = Some note }, Cmd.none
        | OnFieldChanged (note, field) ->
            let updateNote (note: Note) =
                match field with
                | Title title -> { note with Title = title }
                | Content content -> { note with Content = content }

            let state =
                match note with
                | Some note ->
                    let notes =
                        state.Notes
                        |> List.map (fun item -> if item.Id = note.Id then (updateNote note) else item)

                    { state with Notes = notes }
                | None ->
                    let note =
                        match state.CurrentNote with
                        | Some note -> Some(updateNote note)
                        | None -> None

                    { state with CurrentNote = note }

            state, Cmd.none
        | FetchResult result ->
            match result with
            | Ok result -> { state with Notes = result }, Cmd.none
            | Error err ->
                eprintfn "%O" err
                state, Cmd.none
        | SaveNote note ->
            let isCurrentNote =
                match state.CurrentNote with
                | Some current -> note = current
                | None -> false

            match isCurrentNote with
            | true ->
                state,
                Cmd.batch
                    [ Cmd.OfPromise.perform saveNote note SaveResult
                      Cmd.ofMsg NewNote ]
            | false -> state, Cmd.OfPromise.perform saveNote note SaveResult
        | SaveResult result ->
            match result with
            | Ok result ->
                let state =
                    match result with
                    | Some note ->
                        let notes =
                            match state.Notes |> List.contains (note) with
                            | true -> state.Notes
                            | false -> note :: state.Notes

                        { state with Notes = notes }
                    | None -> state

                state, Cmd.none
            | Error err ->
                eprintfn "%O" err
                state, Cmd.none

    let currentNoteForm state dispatch = Html.form []


    let notesList state dispatch = Html.ul []


    let private notes' =
        React.functionComponent
            ("NotesPage",
             (fun () ->
                 let state, dispatch = React.useElmish (init, update, [||])
                 Html.article
                     [ prop.className "page notes-page"
                       prop.children
                           [ currentNoteForm state dispatch
                             notesList state dispatch ] ]))

    let page = notes' ()
