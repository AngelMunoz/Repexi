namespace Mandadinfs

module Main =

    open Fable.Core.JsInterop

    importAll "spectre.css/dist/spectre.min.css"
    importAll "./styles/main.scss"

    open Elmish
    open Elmish.React
#if DEBUG
    open Elmish.Debug
#endif

    let notificationSub (_: App.State) =
        let sub (dispatch: App.Msg -> unit) =
            let onNotification notification =
                dispatch (App.Msg.ShowNotification notification)

            Listeners.notificationHub.Publish.Subscribe(onNotification)
            |> ignore

        Cmd.ofSub sub

    // App
    Program.mkProgram App.init App.update App.render
    |> Program.withSubscription notificationSub
#if DEBUG
    |> Program.withDebugger
    |> Program.withConsoleTrace
#endif
    |> Program.withReactBatched "repexi-app"
    |> Program.run
