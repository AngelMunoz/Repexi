namespace Mandadinfs

module Types =
    open System

    type UserId = UserId of string

    type IngredientAmount = private | IngredientAmount of Amount: float * Unit: string


    type Note =
        { Id: Guid
          User: UserId
          Timestamp: int64
          Title: Option<string>
          Content: string }

    type Ingredient =
        { Name: string
          Amount: IngredientAmount
          Replacement: Option<Ingredient> }

    type Precondition =
        { Content: string
          Order: int
          Notes: string }

    type RecipeStep =
        { Order: int
          Image: Option<string>
          Content: string
          SubStep: RecipeStep
          Notes: string }

    type Recipe =
        { Id: Guid
          Timestamp: int64
          User: UserId
          Title: string
          Preconditions: list<Precondition>
          Ingredients: list<Ingredient>
          Steps: list<RecipeStep>
          Image: Option<string> }
