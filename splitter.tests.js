module.exports = {
    entry: "client/tests/Tests.fsproj",
    outDir: "client/tests-js",
    babel: {
        sourceMaps: false,
        presets: [
            ["@babel/preset-react"],
            ["@babel/preset-env", { modules: "commonjs" }]
        ]
    }
}